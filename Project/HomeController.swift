//
//  ViewController.swift
//  Project
//
//  Created by iosdev on 27.2.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    
    // Labels for the title of the app
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var history: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Display title with a personnal font
        street.font = UIFont(name: "MoonFlower", size: 70.0)
        history.font = UIFont(name: "MoonFlower", size: 70.0)
        
        // Managing the background image
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "eiffel")
        backgroundImage.isHighlighted = true
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        
        // Managing the blur effect for the background
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.prominent)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = UIScreen.main.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0.5
        blurEffectView.insertSubview(backgroundImage, at: 0)
        self.view.insertSubview(blurEffectView, at: 0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    

}

