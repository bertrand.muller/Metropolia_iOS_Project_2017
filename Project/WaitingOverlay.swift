//
//  WaitingOverlay.swift
//  Project
//
//  Created by iosdev on 15.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit

class WaitingOverlay {

    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    class var shared: WaitingOverlay {
        struct Static {
            static let instance: WaitingOverlay = WaitingOverlay()
        }
        return Static.instance
    }
    
    public func showOverlay(view: UIView!) {
        overlayView = UIView(frame: UIScreen.main.bounds)
        overlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityIndicator.center = overlayView.center
        overlayView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        view.addSubview(overlayView)
    }
    
    public func hideOverlayView() {
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
    }
    
}
