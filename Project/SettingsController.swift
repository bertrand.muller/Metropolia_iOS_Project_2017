//
//  SettingsController.swift
//  Project
//
//  Created by iosdev on 1.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit

class SettingsController: UITableViewController {
    
    // Outlets
    @IBOutlet weak var style1: UIImageView!
    @IBOutlet weak var style2: UIImageView!
    @IBOutlet weak var style3: UIImageView!
    @IBOutlet weak var style4: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Applying images
        style1.image = UIImage(named: "mapStandard")
        style2.image = UIImage(named: "mapSilver")
        style3.image = UIImage(named: "mapRetro")
        style4.image = UIImage(named: "mapNight")
        
        // Rounding corners
        style1.layer.cornerRadius = 5
        style2.layer.cornerRadius = 5
        style3.layer.cornerRadius = 5
        style4.layer.cornerRadius = 5
        style1.layer.masksToBounds = true
        style2.layer.masksToBounds = true
        style3.layer.masksToBounds = true
        style4.layer.masksToBounds = true
        
        // Setting border according to the map style in the user defaults
        applyBorder()
        
        // Applying tap gestures for each Image View
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(standardClicked(tapGestureRecognizer:)))
        style1.isUserInteractionEnabled = true
        style1.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(silverClicked(tapGestureRecognizer:)))
        style2.isUserInteractionEnabled = true
        style2.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(retroClicked(tapGestureRecognizer:)))
        style3.isUserInteractionEnabled = true
        style3.addGestureRecognizer(tapGestureRecognizer3)
        
        let tapGestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(nightClicked(tapGestureRecognizer:)))
        style4.isUserInteractionEnabled = true
        style4.addGestureRecognizer(tapGestureRecognizer4)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /**
    * Tag gesture for the style "Standard"
    */
    func standardClicked(tapGestureRecognizer: UITapGestureRecognizer) {
        UserDefaults.standard.set("standard", forKey: "mapStyle")
        applyBorder()
    }
    
    
    /**
     * Tag gesture for the style "Silver"
     */
    func silverClicked(tapGestureRecognizer: UITapGestureRecognizer) {
        UserDefaults.standard.set("silver", forKey: "mapStyle")
        applyBorder()
    }
    
    
    /**
     * Tag gesture for the style "Retro"
     */
    func retroClicked(tapGestureRecognizer: UITapGestureRecognizer) {
        UserDefaults.standard.set("retro", forKey: "mapStyle")
        applyBorder()
    }
    
    
    /**
     * Tag gesture for the style "Night"
     */
    func nightClicked(tapGestureRecognizer: UITapGestureRecognizer) {
        UserDefaults.standard.set("night", forKey: "mapStyle")
        applyBorder()
    }
    
    
    /**
    * Method used to create a border to show the style currently used
    */
    func applyBorder() {
        
        let mapStyle = UserDefaults.standard.string(forKey: "mapStyle")!
        style1.layer.borderWidth = 0
        style2.layer.borderWidth = 0
        style3.layer.borderWidth = 0
        style4.layer.borderWidth = 0
        
        switch(mapStyle) {
            case "standard":
                style1.layer.borderWidth = 2
                style1.layer.borderColor = UIColor.green.cgColor
            break
            
            case "silver":
                style2.layer.borderWidth = 2
                style2.layer.borderColor = UIColor.green.cgColor
            break
            
            case "retro":
                style3.layer.borderWidth = 2
                style3.layer.borderColor = UIColor.green.cgColor
            break
            
            case "night":
                style4.layer.borderWidth = 2
                style4.layer.borderColor = UIColor.green.cgColor
            break
            
            default: break
            
        }
        
    }
    
}
