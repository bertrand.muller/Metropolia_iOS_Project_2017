//
//  AppDelegate.swift
//  Project
//
//  Created by iosdev on 27.2.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let container = NSPersistentContainer(name: "Model")

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GMSPlacesClient.provideAPIKey("AIzaSyB0-qyRRplEHHiNgxX8r8CmrRHwk_e3YP0")
        GMSServices.provideAPIKey("AIzaSyB0-qyRRplEHHiNgxX8r8CmrRHwk_e3YP0")
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error: \(error)")
            } else {
                
            }
        })
        
        // Removing previous Building
        let context = container.viewContext
        Building.emptyBuildings(context: context)
        
        do {
            try context.save()
        } catch {}
        
        // Store a default map style if not defined
        guard (UserDefaults.standard.string(forKey: "mapStyle") != nil) else {
            UserDefaults.standard.set("retro", forKey: "mapStyle")
            return true
        }
        
        return true
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }

}

