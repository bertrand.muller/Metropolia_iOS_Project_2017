//
//  InformationViewController.swift
//  Project
//
//  Created by iosdev on 14.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameBuilding: UILabel!
    @IBOutlet weak var streetTextField: UILabel!
    @IBOutlet weak var contactTextField: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelContact: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Search for a picture in Google API
        imageView.image = PageViewController.photosBuilding[2]
        
        // Customizing the title
        nameBuilding.font = UIFont(name: "MoonFlower", size: 48.0)
        nameBuilding.text = "Information"
        nameBuilding.lineBreakMode = NSLineBreakMode.byWordWrapping
        nameBuilding.numberOfLines = 2
        
        // Customizing the street text field
        let street = PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["street"])
        let city = PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["city"])
        let postalCode = PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["postalCode"])
        let country = PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["country"])
        streetTextField.text = "\(street)\n\(city) \(postalCode)\n\(country)"
        
        streetTextField.textColor = UIColor.black
        streetTextField.layer.cornerRadius = 6.0
        streetTextField.layer.masksToBounds = true
        streetTextField.textAlignment = NSTextAlignment.left;
        streetTextField.numberOfLines = 0
        streetTextField.sizeToFit()
        
        // Customizing contact text field
        let phone = PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["phone"])
        let website = PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["website"])
        contactTextField.text = "\(phone)\n\(website)"
        
        // Verifying if there is contact information
        if contactTextField.text == "\n" {
            labelContact.isHidden = true
        }
        
        contactTextField.textColor = UIColor.black
        contactTextField.layer.cornerRadius = 6.0
        contactTextField.layer.masksToBounds = true
        contactTextField.textAlignment = NSTextAlignment.left;
        contactTextField.numberOfLines = 0
        contactTextField.sizeToFit()
        
        // Customizing labels' font
        labelAddress.font = UIFont(name: "Publicite", size: 25.0)
        labelContact.font = UIFont(name: "Publicite", size: 25.0)
        
        // Setting the title
        self.title = "Details"
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
