//
//  GoogleMapsController.swift
//  Project
//
//  Created by iosdev on 2.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import CoreLocation
import CoreData

class GoogleMapsController: UIViewController, CLLocationManagerDelegate, UIGestureRecognizerDelegate, GMSMapViewDelegate {
    
    // Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var toolbarDesc: UIToolbar!
    @IBOutlet weak var toolbarShare: UIToolbar!
    @IBOutlet weak var textFieldShare: UITextField!
    @IBOutlet weak var textFieldDesc: UITextField!
    @IBOutlet weak var buttonNO: UIButton!
    @IBOutlet weak var buttonYES: UIButton!
    
    // Attributes
    private let manager = CLLocationManager()
    private var placesClient: GMSPlacesClient!
    private var imageInfoWindow = UIImage()
    private var infosMarkerForPageView = [String: Any]()
    private var markersDisplayOnce = false
    private var context: NSManagedObjectContext!
    var infosLongPressure = [String: Any]()
    var markerLongPress = GMSMarker()
    var overlay = WaitingOverlay()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Getting the context for Core Data
        context = (UIApplication.shared.delegate as! AppDelegate).container.viewContext
        
        // Initialization of the toolbar for the description
        toolbarDesc.layer.zPosition = 2
        toolbarDesc.isUserInteractionEnabled = true
        textFieldDesc.borderStyle = UITextBorderStyle.none
        
        // Initialization of the toolbar for sharing
        toolbarShare.layer.zPosition = 2
        toolbarShare.isUserInteractionEnabled = true
        toolbarShare.isHidden = true
        textFieldShare.isEnabled = false
        textFieldShare.borderStyle = UITextBorderStyle.none
        
        // Initialization of the parameters to get the user location
        placesClient = GMSPlacesClient.shared()
        manager.requestWhenInUseAuthorization()
        
        // Initialization of the Google Maps
        mapView.isMyLocationEnabled = true
        mapView.accessibilityElementsHidden = false
        mapView.settings.myLocationButton = true
        mapView.padding = UIEdgeInsetsMake(0, 0, 5, 30)
        mapView.layer.zPosition = -1
        
        // Centering the map
        centerMapAccordingToLocation()
        
        // Add the delegates
        manager.delegate = self
        mapView.delegate = self
        
        // Setting the title of the page
        self.title = "Map"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /**
    * Method used to apply the style of the map chosen by the user
    */
    func applyStyleMap() {
        
        do {
            
            let mapStyle = UserDefaults.standard.string(forKey: "mapStyle")!
            
            if let styleURL = Bundle.main.url(forResource: "\(mapStyle)MapStyle", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            }
            
        } catch {}
        
    }
    
    
    /**
    * Method called each time the map finishes to render
    */
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        applyStyleMap()
        placingMarkersOnMap(overlay: false)
    }
    
    
    /**
    * Method used to show the markers on the map
    */
    func placingMarkersOnMap(overlay: Bool) {
        
        // Variables used to determine if a marker has to be shown or not
        let coorNorthEast = mapView.projection.coordinate(for: CGPoint(x: mapView.bounds.width, y: 0))
        let coorSouthWest = mapView.projection.coordinate(for: CGPoint(x: 0, y: mapView.bounds.height))
        
        // Creation of the URL for getting data according to the south west and north east coordinates of the viewport
        var url = "https://beber.000webhostapp.com/allBuildingsInRegion?"
        url += "northEastLatitude=\(coorNorthEast.latitude)&"
        url += "northEastLongitude=\(coorNorthEast.longitude)&"
        url += "southWestLatitude=\(coorSouthWest.latitude)&"
        url += "southWestLongitude=\(coorSouthWest.longitude)"
        
        // Creation of the session to do the request
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.url = NSURL(string: url) as URL?
        
        session.dataTask(with: request.url!, completionHandler: {(data, response, error) -> Void in
            
            if let error = error {
                print(error.localizedDescription)
            } else {
                
                do {
                    
                    if let feedList = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any] {
                    
                        // Analyzing JSON data sent by the server
                        if let category = feedList["countries"] as? [String: AnyObject] {
                            for country in category {
                                if let buildings = country.value as? [AnyObject] {
                                    for building in buildings {
                                        if let data = building as? [String: AnyObject] {
                                       
                                            // Creating the marker according to the data
                                            if let lat = data["latitude"], let long = data["longitude"] {
                                                if let latVerified = lat as? CLLocationDegrees, let longVerified = long as? CLLocationDegrees {
                                                    let coordinate = CLLocationCoordinate2DMake(latVerified, longVerified)
                                                
                                                    DispatchQueue.main.async {
                                                        self.placeMarker(coordinate: coordinate, infos: data, overlay: overlay)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    
                        self.markersDisplayOnce = true
                    }
                    
                } catch {
                }
                
            }
            
            self.mapView.frame = CGRect(x: 0, y: 0, width: self.mapView.frame.width, height: 647.0)
            
        }).resume()
        
    }
    
    
    /**
    * Function used to add a marker on the map accoridng to the coordinates
    */
    func placeMarker(coordinate: CLLocationCoordinate2D, infos: [String: AnyObject], overlay: Bool) {
        
        if(!Building.verifyPositionExists(context: context, latitude: coordinate.latitude, longitude: coordinate.longitude)) {
            
            // Creation of the corresponding marker
            let marker = GMSMarker(position: coordinate)
            marker.position = coordinate
            marker.title = infos["name"] as! String?
            marker.snippet = infos["street"] as! String?
            marker.icon = GMSMarker.markerImage(with: UIColor.init(red: 113/255.0, green: 179/255.0, blue: 0, alpha: 1.0))
            marker.isFlat = true
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.iconView = UIImageView(image: UIImage(named: "pin"))
            marker.map = mapView
            
            // Add the position of the new marker to Core Data
            Building.addPositionMarker(context: context, latitude: coordinate.latitude, longitude: coordinate.longitude)
            
            do {
                try context.save()
            } catch {
                print("Error with context")
            }
            
        }
        
    }
    
    
    /*
    * Function used to center the map with the current user location
    */
    func centerMapAccordingToLocation() {
        
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 14)
                    self.mapView.animate(to: camera);
                }
            }
        })
        
    }
    
    
    /**
    * Method used to verify if an attribute of an address has a value
    */
    func getValueOfAddress(value: Any?) -> Any {
        
        if let val = value {
            return val
        } else {
            return ""
        }
        
    }
    
    
    /**
    * Method used to get the information about a place according to its coordinates
    */
    func reverseGeocodingToGetInformation(coordinate: CLLocationCoordinate2D, longPressure: Bool) {
        
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate, completionHandler: {(response, error) in
            if let address = response?.firstResult() {
                if(longPressure) {
                    self.infosLongPressure["latitude"] = Double(coordinate.latitude)
                    self.infosLongPressure["longitude"] = Double(coordinate.longitude)
                    self.infosLongPressure["street"] = self.getValueOfAddress(value: address.thoroughfare)
                    self.infosLongPressure["postalCode"] = self.getValueOfAddress(value: address.postalCode)
                    self.infosLongPressure["city"] = self.getValueOfAddress(value: address.locality)
                    self.infosLongPressure["country"] = self.getValueOfAddress(value: address.country)
                }
            }
        })
        
    }
    
    
    /**
    * Method used to put a temporary marker after a long press on the map
    */
    func addMarkerForLastDataSaved() {
        
        // Getting the coordinates
        let latitude = infosLongPressure["latitude"] as! CLLocationDegrees
        let longitude = infosLongPressure["longitude"] as! CLLocationDegrees
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        // Creation of the corresponding marker
        let marker = GMSMarker(position: coordinate)
        marker.position = coordinate
        marker.title = "Location pressed"
        marker.snippet = "test"
        marker.map = mapView
        
    }
    
    
    /**
    * Method called when the user presses the map to add a marker
    */
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        
        // Getting information about the place according to the coordinates
        reverseGeocodingToGetInformation(coordinate: coordinate, longPressure: true)
        
        // Creation of the corresponding marker
        let marker = GMSMarker(position: coordinate)
        marker.position = coordinate
        marker.title = "Location pressed"
        marker.snippet = "test"
        marker.map = mapView
        markerLongPress = marker
        
        // Switching between toolbars
        toolbarDesc.isHidden = true
        toolbarShare.isHidden = false
        
    }
    
    
    /**
    * Method used to prepare the segue (display page view controller)
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "details") {
            
            // Send data to the page view controller
            PageViewController.infosPlace = infosMarkerForPageView
            
            // Changing the naviagation controller's top bar
            let backItem = UIBarButtonItem()
            backItem.title = "Map"
            navigationItem.backBarButtonItem = backItem
            
        }
        
    }
    
    
    /**
    * Method called when the user is clicking on the marker
    * (preparing the data in case the user clicks on the info window)
    */
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        prepareInfosPlaceForPageView(latitude: marker.position.latitude, longitude: marker.position.longitude)
        return false
    }
    
    
    /**
    * Method called when the user clicks on the infor window of a marker
    */
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        performSegue(withIdentifier: "details", sender: self)
    }
    
    
    /**
    * Method used to prepare the information to send to the page view controller
    */
    func prepareInfosPlaceForPageView(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        // Creation of the URL for getting data according to the south west and north east coordinates of the viewport
        var url = "https://beber.000webhostapp.com/building?"
        url += "latitude=\(latitude)&"
        url += "longitude=\(longitude)"
        
        // Creation of the session to do the request
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.url = NSURL(string: url) as URL?
        
        session.dataTask(with: request.url!, completionHandler: {(data, response, error) -> Void in
            
            if let error = error {
                print(error.localizedDescription)
            } else {
                
                do {
                    
                    // Getting the data from the server
                    self.infosMarkerForPageView = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: Any]
                    
                    DispatchQueue.main.async {
                        // Search for pictures in Google API
                        let placeID = PageViewController.verifyValueInInfosPlace(value: self.infosMarkerForPageView["placeID"]) as! String
                        PageViewController.loadPhotosForPlace(placeID: placeID)
                    }
                    
                    
                } catch {}
                
            }
            
        }).resume()
        
    }
    
    
    /**
    * Action used to display a pop up over the map
    */
    @IBAction func buttonYesPressed(_ sender: Any) {
        
        // Creation of the pop up when user is pressing "YES" in the toolbar
        let popUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUp") as! PopUpViewController
        addChildViewController(popUp)
        popUp.view.frame = view.frame
        view.addSubview(popUp.view)
        popUp.didMove(toParentViewController: self)
        popUp.mapController = self
        
        // Removing temporary marker
        markerLongPress.map = nil
        
        // Switching between toolbars
        toolbarShare.isHidden = true
        toolbarDesc.isHidden = false
        
    }
    
    @IBAction func buttonNoPressed(_ sender: Any) {
        
        // Cancelling action of adding a new place
        markerLongPress.map = nil
        toolbarShare.isHidden = true
        toolbarDesc.isHidden = false
        
    }
    
}

