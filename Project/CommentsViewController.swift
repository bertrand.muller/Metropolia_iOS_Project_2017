//
//  CommentsViewController.swift
//  Project
//
//  Created by iosdev on 15.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController {

    // Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var logoLetter1: UILabel!
    @IBOutlet weak var logoLetter2: UILabel!
    @IBOutlet weak var logoLetter3: UILabel!
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var comment1: UITextView!
    @IBOutlet weak var comment2: UITextView!
    @IBOutlet weak var comment3: UITextView!
    @IBOutlet weak var rating1: UILabel!
    @IBOutlet weak var rating2: UILabel!
    @IBOutlet weak var rating3: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Verifying if a comment exists
        if let comment1Data = verifyValueInInfosPlace(value: PageViewController.infosPlace["0"] as! [String : Any]?) {
            
            // Customizing text field comment1
            comment1.textAlignment = NSTextAlignment.justified
            comment1.isScrollEnabled = true
            comment1.isEditable = false
            
            // Inserting data
            name1.text = comment1Data["author"] as! String?
            comment1.text = comment1Data["text"] as! String?
            rating1.text = comment1Data["rating"] as! String?
            
            // Customizing logo letter
            logoLetter1.layer.cornerRadius = 15.0
            logoLetter1.layer.masksToBounds = true
            if let c = name1.text?.characters.first { logoLetter1.text = "\(c)" }
            logoLetter1.textColor = UIColor.white
            
            if comment1Data.count == 0 {
                view1.isHidden = true
            }
            
        }
        
        // Verifying if a comment exists
        if let comment2Data = verifyValueInInfosPlace(value: PageViewController.infosPlace["1"] as! [String: Any]?) {
            
            // Customizing text field comment2
            comment2.textAlignment = NSTextAlignment.justified
            comment2.isScrollEnabled = true
            comment2.isEditable = false
            
            // Inserting data
            name2.text = comment2Data["author"] as! String?
            comment2.text = comment2Data["text"] as! String?
            rating2.text = comment2Data["rating"] as! String?
            
            // Customizing logo letter
            logoLetter2.layer.cornerRadius = 15.0
            logoLetter2.layer.masksToBounds = true
            if let c = name2.text?.characters.first { logoLetter2.text = "\(c)" }
            logoLetter2.textColor = UIColor.white
            
            if comment2Data.count == 0 {
                view2.isHidden = true
            }
            
        }
        
        // Verifying if a comment exists
        if let comment3Data = verifyValueInInfosPlace(value: PageViewController.infosPlace["2"] as! [String: Any]?) {
            
            // Customizing text field comment2
            comment3.textAlignment = NSTextAlignment.justified
            comment3.isScrollEnabled = true
            comment3.isEditable = false
            
            // Inserting data
            name3.text = comment3Data["author"] as! String?
            comment3.text = comment3Data["text"] as! String?
            rating3.text = comment3Data["rating"] as! String?
            
            // Customizing logo letter
            logoLetter3.layer.cornerRadius = 15.0
            logoLetter3.layer.masksToBounds = true
            if let c = name3.text?.characters.first { logoLetter3.text = "\(c)" }
            logoLetter3.textColor = UIColor.white
         
            if comment3Data.count == 0 {
                view3.isHidden = true
            }
            
        }
        
        // Search for a picture in Google API
        imageView.image = PageViewController.photosBuilding[1]
        
        // Customizing title label
        commentsLabel.font = UIFont(name: "MoonFlower", size: 55.0)
        commentsLabel.text = "Reviews"
        commentsLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        commentsLabel.numberOfLines = 3
        
        // Setting the title
        self.title = "Details"
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /**
     * Verify if a comment in the array infosPlace is set
     */
    func verifyValueInInfosPlace(value: [String: Any]?) -> [String: Any]? {
        
        if let v = value {
            return v
        }
        
        return [String: Any]()
        
    }
    
}
