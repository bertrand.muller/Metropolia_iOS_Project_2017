//
//  PageViewController.swift
//  Project
//
//  Created by iosdev on 13.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit
import GooglePlaces

class PageViewController: UIPageViewController, UIPageViewControllerDataSource {
    
    // Attributes
    static var infosPlace = [String: Any]()
    static var photosBuilding = [Int: UIImage]()
    
    private lazy var orderedViewControllers:[UIViewController] = {
        return [self.getViewController(name: "First"),
                self.getViewController(name: "Second"),
                self.getViewController(name: "Third")]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the data source
        dataSource = self
        
        
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /**
    * Method used to create a view controller according to a specific name
    */
    private func getViewController(name: String) -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(name)ViewController")
        return view
    }

    
    /**
    * Method used to get the view controller before the current one
    */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
        
    }
    
    
    /**
    * Method used to get the view controller after the current one 
    */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
 
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
        
    }
    
    
    /**
     * Method used to search for a picture linked according to the place id
     */
    static func loadPhotosForPlace(placeID: String) {
        
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: placeID, callback: { (photos, error) -> Void in
            if error == nil {
                
                    // Test for the number of pictures
                    if (photos?.results.count)! == 1 {
                        self.loadImageForMetadata(photoMetaData: (photos?.results[0])!, index: 0)
                        PageViewController.photosBuilding[1] = UIImage(named: "loi3")!
                        PageViewController.photosBuilding[2] = UIImage(named: "loi3")!
                    } else if (photos?.results.count)! == 2 {
                        self.loadImageForMetadata(photoMetaData: (photos?.results[0])!, index: 0)
                        self.loadImageForMetadata(photoMetaData: (photos?.results[1])!, index: 1)
                        PageViewController.photosBuilding[2] = UIImage(named: "loi3")!
                    } else if (photos?.results.count)! >= 3 {
                        self.loadImageForMetadata(photoMetaData: (photos?.results[0])!, index: 0)
                        self.loadImageForMetadata(photoMetaData: (photos?.results[1])!, index: 1)
                        self.loadImageForMetadata(photoMetaData: (photos?.results[2])!, index: 2)
                    } else {
                        PageViewController.photosBuilding[0] = UIImage(named: "loi3")!
                        PageViewController.photosBuilding[1] = UIImage(named: "loi3")!
                        PageViewController.photosBuilding[2] = UIImage(named: "loi3")!
                    }
                
            }
        })
        
    }
    
    
    /**
     * Method used to display a photo at the top of the view
     */
    static func loadImageForMetadata(photoMetaData: GMSPlacePhotoMetadata, index: Int) {
        
        GMSPlacesClient.shared().loadPlacePhoto(photoMetaData, callback: { (photo, error) -> Void in
           PageViewController.photosBuilding[index] = photo!
        })
        
    }


    /**
     * Verify if a value in the array infosPlace is set
     */
    static func verifyValueInInfosPlace(value: Any?) -> Any {
        
        if let v = value {
            return v
        }
        
        return ""
        
    }
    
}
