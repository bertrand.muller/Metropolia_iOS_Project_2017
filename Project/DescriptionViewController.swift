//
//  TestViewController.swift
//  Project
//
//  Created by iosdev on 13.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit
import GooglePlaces

class DescriptionViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var imageTop: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var nameBuilding: UILabel!
    @IBOutlet weak var labelHistory: UILabel!
    @IBOutlet weak var edit: UIImageView!
    @IBOutlet weak var stars: UILabel!
    @IBOutlet weak var buttonSend: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customizing the description text view
        descriptionTextView.text = "\(PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["description"]))"
        descriptionTextView.textColor = UIColor.init(red: 132/255.0, green: 137/255.0, blue: 140/255.0, alpha: 1.0)
        descriptionTextView.textAlignment = NSTextAlignment.justified
        descriptionTextView.isEditable = false
        
        // Customizing the title 
        nameBuilding.font = UIFont(name: "MoonFlower", size: 55.0)
        nameBuilding.text = PageViewController.infosPlace["name"] as! String?
        nameBuilding.lineBreakMode = NSLineBreakMode.byWordWrapping
        nameBuilding.numberOfLines = 3
        
        // Customizing rating
        stars.text = "\(PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["rating"]))"
        
        // Customizing label history
        labelHistory.font = UIFont(name: "Publicite", size: 40.0)
        
        // Customizing button send
        buttonSend.isHidden = true
        buttonSend.layer.cornerRadius = 7.0
        buttonSend.layer.masksToBounds = true
        buttonSend.layer.borderWidth = 1.0
        
        // Edit image view
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(editClicked(tapGestureRecognizer:)))
        edit.isUserInteractionEnabled = true
        edit.addGestureRecognizer(tapGestureRecognizer)
        
        //Setting the title
        self.title = "Details"
        
        // Setting the image
        imageTop.image = PageViewController.photosBuilding[0]
        navigationController?.navigationBar.isTranslucent = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /**
    * Method used when the edit image view is clicked
    */
    func editClicked(tapGestureRecognizer: UITapGestureRecognizer) {
        
        descriptionTextView.isEditable = !descriptionTextView.isEditable
        descriptionTextView.textColor = UIColor.red
        buttonSend.isHidden = !buttonSend.isHidden
        
        if(buttonSend.isHidden) {
            descriptionTextView.textColor = UIColor.init(red: 132/255.0, green: 137/255.0, blue: 140/255.0, alpha: 1.0)
        }
        
    }
    
    
    /**
    * Method used to post the historical description modified
    */
    func sendModification() {
        
        // Creation of the URL for posting data
        let latitude = PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["latitude"])
        let longitude = PageViewController.verifyValueInInfosPlace(value: PageViewController.infosPlace["longitude"])
        var url = "https://beber.000webhostapp.com/updateDesc?latitude=\(latitude)&longitude=\(longitude)&description=\(descriptionTextView.text!)"
        
        let unreservedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/?&=.:"
        let unreservedCharsSet = CharacterSet(charactersIn: unreservedChars)
        url = url.addingPercentEncoding(withAllowedCharacters: unreservedCharsSet)!
        
        // Creation of the session to do the request
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.url = NSURL(string: url) as URL?
        
        session.dataTask(with: request.url!, completionHandler: {(data, response, error) -> Void in
            //
        }).resume()
        
    }
    
    @IBAction func sebdButtonPressed(_ sender: Any) {
        sendModification()
        buttonSend.isHidden = true
        descriptionTextView.isEditable = false
        descriptionTextView.textColor = UIColor.init(red: 132/255.0, green: 137/255.0, blue: 140/255.0, alpha: 1.0)
    }

}
