//
//  PopUpViewController.swift
//  Project
//
//  Created by iosdev on 11.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit
import GoogleMaps

class PopUpViewController: UIViewController {

    // Outlets
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var titlePopUp: UILabel!
    @IBOutlet weak var subTitlePopUp: UILabel!
    @IBOutlet weak var namePlace: UITextField!
    @IBOutlet weak var descPlace: UITextView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    // Attributes
    var mapController = GoogleMapsController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hidding everything first (for fade in animation)
        view.alpha = 0
        popUpView.alpha = 0
        titlePopUp.alpha = 0
        subTitlePopUp.alpha = 0
        namePlace.alpha = 0
        descPlace.alpha = 0
        okButton.alpha = 0
        cancelButton.alpha = 0
        
        // Customizing title and subtitle
        titlePopUp.font = UIFont(name: "MoonFlower", size: 30.0)
        titlePopUp.textColor = UIColor.white
        subTitlePopUp.textColor = UIColor.white
        
        // Customizing view
        view.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        
        // Customizing pop up
        popUpView.layer.masksToBounds = true
        popUpView.layer.cornerRadius = 10
        popUpView.backgroundColor = UIColor.init(colorLiteralRed: 128/255.0, green: 145/255.0, blue: 143/255.0, alpha: 0.9)
        
        // Customizing field for the name of the place
        namePlace.layer.addSublayer(getBorder(element: namePlace, position: 4, alpha: true))
        namePlace.layer.masksToBounds = true
        namePlace.backgroundColor = UIColor.init(colorLiteralRed: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
        namePlace.placeholder = "Place Name"
        namePlace.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: namePlace.frame.height))
        namePlace.leftViewMode = UITextFieldViewMode.always
        namePlace.textColor = UIColor.white
        
        // Customizing field for the description
        descPlace.layer.addSublayer(getBorder(element: descPlace, position: 2, alpha: true))
        descPlace.layer.addSublayer(getBorder(element: descPlace, position: 4, alpha: true))
        descPlace.text = "Describe the place from a historical point of view..."
        descPlace.font = UIFont(name: descPlace.font!.fontName, size: 10)
        descPlace.backgroundColor = UIColor.init(colorLiteralRed: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
        
        // Customizing buttons
        okButton.tintColor = UIColor.white
        cancelButton.tintColor = UIColor.white
        
        // Initialization of the popUpView
        popUpView.layer.zPosition = 3
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fade(inEffect: true)
    }
    
    
    /**
    * Method used to apply borders for an UIView
    */
    func getBorder(element: UIView, position: Int, alpha: Bool) -> CALayer {
        
        let border = CALayer()
        let width = CGFloat(1.0)
        var rect = CGRect()
        
        switch position {
            
        case 1:
            rect = CGRect(x: 0, y: 0, width: element.frame.size.width, height: width)
            
        case 2:
            rect = CGRect(x: element.frame.size.width - width, y: 0, width: width, height: element.frame.size.height)
            
        case 3:
            rect = CGRect(x: 0, y: element.frame.size.height - width, width: element.frame.size.width, height: element.frame.size.height)
            
        case 4:
            rect = CGRect(x: 0, y: 0, width: width, height: element.frame.size.height)
            
        default:
            rect = CGRect(x: 0, y: 0, width: 0, height: 0)
            
        }
        
        if alpha {
            border.borderColor = UIColor.white.withAlphaComponent(0.4).cgColor
        } else {
            border.borderColor = UIColor.white.cgColor
        }
        
        border.frame = rect
        border.borderWidth = width
        return border
        
    }
    
    
    /**
    * Method used to make the pop up appearing with a fadeIn effect
    */
    func fade(inEffect: Bool) {
        
        let alpha = CGFloat((inEffect ? 1.0 : 0.0))
        
        UIView.animate(withDuration: 0.7, animations: {
            self.view.alpha = alpha
            self.popUpView.alpha = alpha
            self.titlePopUp.alpha = alpha
            self.subTitlePopUp.alpha = alpha
            self.namePlace.alpha = alpha
            self.descPlace.alpha = alpha
            self.okButton.alpha = alpha
            self.cancelButton.alpha = alpha
        }) { _ in
            if !inEffect { self.view.removeFromSuperview() }
        }
        
    }
    
    
    /**
    * Method used to send the data to the server
    */
    func savePlaceInDatabase() {
        
        // Creation of the URL for posting data
        var url = "https://beber.000webhostapp.com/buildingPost?"
        
        for (key, value) in mapController.infosLongPressure {
            url += "\(key)=\(value)&"
        }
        
        var urlModified = url.substring(to: url.index(before: url.endIndex))
        let unreservedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/?&=.:"
        let unreservedCharsSet = CharacterSet(charactersIn: unreservedChars)
        urlModified = urlModified.addingPercentEncoding(withAllowedCharacters: unreservedCharsSet)!
        
        // Creation of the session to do the request
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.url = NSURL(string: urlModified) as URL?
        
        session.dataTask(with: request.url!, completionHandler: {(data, response, error) -> Void in
            //
        }).resume()
        
    }
    
    @IBAction func okButtonPressed(_ sender: Any) {
        
        fade(inEffect: false)
        
        // Overlay
        /*let overlay = WaitingOverlay()
        overlay.showOverlay(view: mapController.view)
        mapController.overlay = overlay*/
        
        mapController.infosLongPressure["name"] = namePlace.text
        mapController.infosLongPressure["description"] = descPlace.text
        savePlaceInDatabase()
        
        mapController.placingMarkersOnMap(overlay: true)
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        fade(inEffect: false)
        mapController.markerLongPress.map = nil
    }

}
