//
//  Building+CoreDataClass.swift
//  Project
//
//  Created by iosdev on 13.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import Foundation
import CoreData


public class Building: NSManagedObject {

    
    /**
    * Method used to add a new marker to Core Data
    */
    static func addPositionMarker(context: NSManagedObjectContext, latitude: Double, longitude: Double) {
        
        let b = NSEntityDescription.insertNewObject(forEntityName: "Building", into: context) as! Building
        b.latitude = latitude
        b.longitude = longitude
        
    }
    
    
    /**
    * Method used to verify if a coordinate is already existing in the database
    * (to avoid to display a new marker over the one already existing)
    */
    static func verifyPositionExists(context: NSManagedObjectContext, latitude: Double, longitude: Double) -> Bool {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Building")
        fetchRequest.predicate = NSPredicate(format: "latitude == \(latitude) AND longitude == \(longitude)")
        var result = [Building]()
        
        do {
            result = try context.fetch(fetchRequest) as! [Building]
        } catch {}
        
        let val = (result.count > 0)
        return val
        
    }
    
    
    /**
    * Method used to delete all the coordinates saved before
    * (when application launched)
    */
    static func emptyBuildings(context: NSManagedObjectContext) {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Building")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try context.execute(deleteRequest)
        } catch {}

    }
    
}
