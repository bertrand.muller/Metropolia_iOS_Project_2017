//
//  Building+CoreDataProperties.swift
//  Project
//
//  Created by iosdev on 13.3.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import Foundation
import CoreData


extension Building {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Building> {
        return NSFetchRequest<Building>(entityName: "Building");
    }

    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double

}
